import React, { useState, useEffect } from 'react';
import { documentTypes } from '../lib/documents';
import DocumentList from './DocumentList';
import Axios from 'axios';

export default function AdminPanel() {
    const [availableDocs, setAvailableDocs] = useState([]);
    const [selectedDocs, setSelectedDocs] = useState([]);
    const [message, setMessage] = useState('');

    useEffect(() => {
        async function getDocuments() {
            const result = await Axios.get('http://localhost/');
            const savedDocs = result.data;
            let unsavedDocs = [];
            documentTypes.forEach(documentType => {
                if (!savedDocs.includes(documentType)) unsavedDocs.push(documentType);
            });
            setSelectedDocs(savedDocs);
            setAvailableDocs(unsavedDocs);
        }
        getDocuments();
    }, []);

    const handleSaveClick = () => {
        Axios.post('http://localhost/', JSON.stringify({ documents: selectedDocs }))
            .then(res => {
                setMessage('Saved!');
            })
            .catch(error => {
                setMessage(error.response.data.message);
            });
    };

    const handleDocumentSelection = (documents, type) => {
        let newDocs = type === 'available' ? availableDocs.slice() : selectedDocs.slice();
        documents.forEach(document => {
            const index = newDocs.indexOf(document);
            newDocs.splice(index, 1);
        });
        if (type === 'available') {
            setAvailableDocs(newDocs);
            setSelectedDocs([...selectedDocs, ...documents]);
        } else {
            setSelectedDocs(newDocs);
            setAvailableDocs([...availableDocs, ...documents]);
        }
    };

    return (
        <div>
            <DocumentList
                label="Available Options"
                documents={availableDocs}
                type={'available'}
                selectDocuments={handleDocumentSelection}
            />
            <DocumentList
                label="Selected Options"
                documents={selectedDocs}
                type={'selected'}
                selectDocuments={handleDocumentSelection}
            />
            <button onClick={handleSaveClick}>Save changes</button>
            <p>{message}</p>
        </div>
    );
}
