import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const ListItem = styled.li`
    background: ${props => (props.selected ? 'grey' : 'white')};
    cursor: pointer;
`;

export default function DocumentList({ documents, type, selectDocuments, label }) {
    const [selected, setSelected] = useState([]);

    useEffect(
        type => {
            if (type === 'selected') {
                setSelected([]);
            }
        },
        [documents],
    );

    const handleItemClick = document => {
        const index = selected.indexOf(document);

        if (index > -1) {
            let newSelected = selected.slice();
            newSelected.splice(index, 1);
            setSelected(newSelected);
        } else {
            setSelected([...selected, document]);
        }
    };

    const handleAddOrRemove = () => {
        selectDocuments(selected, type);
        setSelected([]);
    };

    return (
        <div>
            <h3>{label}</h3>
            <ul>
                {documents.map((document, i) => (
                    <ListItem
                        key={i}
                        onClick={() => handleItemClick(document)}
                        selected={selected.includes(document)}
                    >
                        {document}
                    </ListItem>
                ))}
            </ul>
            <button onClick={handleAddOrRemove}>{type === 'available' ? 'Add' : 'Remove'}</button>
        </div>
    );
}
